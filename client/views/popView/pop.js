Template.popview.rendered = function() {

    Deps.autorun(function() {
        var dateConst=['2014-6-06 00:00:00', '2014-6-07 00:00:00', '2014-6-08 00:00:00', '2014-6-09 00:00:00']

        // for(var i=0;i<3;i++){
            var i=0;
            var timelineData;
            var subscribe_handler = Meteor.subscribe('timeline', i);
            if (subscribe_handler.ready()) {
                timelineData= Collection.timeline.find({
                    time:{$gt:dateConst[i], $lt:dateConst[i+1]}
                }).fetch();
            }

            if(!_.isEmpty(timelineData)){
                //setup heatmap canvas
                var heatmapConf= Template.popview.configure.heatmap;
                heatmapConf.width=$('#popCanvasContainer'+i).height();
                heatmapConf.height=$('#popCanvasContainer'+i).height();

                // d3.select("#heatmap"+i)
                //     .attr("width", heatmapConf.width)
                //     .attr("height", heatmapConf.height);
                var c=document.getElementById("heatmap"+i);
                c.width=heatmapConf.width;
                c.height=heatmapConf.height;

                //setup svg canvas
                var popSvgConf= Template.popview.configure.popSvg;
                d3.select("#popSvg"+i)
                    .attr("width", heatmapConf.width)
                    .attr("height", popSvgConf.height);
                $("#popSvg"+i).empty();

                var popSvgG= d3.select("#popSvg"+i).append('g')
                    .attr("class", "popSvgG"+i)


                //get heatmap data
                var selectedTime=Session.get('selectedTime');
                var heatmap_subscribe_handler = Meteor.subscribe('timePoint', selectedTime);
                var heatmapData;

                var sec=str2sec(selectedTime);
                var day=selectedTime.split(' ')[0];
                var start=sec-15, end=sec+15;

                var istart=day+' '+secondsToTime(start);
                var iend=day+' '+secondsToTime(end);

                if (subscribe_handler.ready()) {
                    heatmapData= Collection.mc1.find({
                        'Timestamp':{$gt:istart, $lt:iend}
                    }).fetch();
                }
                console.log(heatmapData)
                // if(_.isEmpty(heatmapData)) continue;
                //start drawing
                drawPop(popSvgG, timelineData)
                drawHeatmap(c, heatmapData)
            }

        // }

    })

}

var str2sec = function(str) {
    var inputFormat = 'YYYY-M-DD HH:mm:ss'
    var parse = moment(str, inputFormat);
    return parse.hour() * 3600 + parse.minute() * 60 + parse.second();
}
function secondsToTime(secs)
{
    var hour = Math.floor(secs / (60 * 60));
   
    var divisor_for_minutes = secs % (60 * 60);
    var minute = Math.floor(divisor_for_minutes / 60);
 
    var divisor_for_seconds = divisor_for_minutes % 60;
    var second = Math.ceil(divisor_for_seconds);
   
    if (hour < 10) hour = '0' + hour;
    if (minute < 10) minute = '0' + minute;
    if (second < 10) second = '0' + second;
    return '' + hour + ':' + minute + ':' + second;
}

var drawHeatmap=function(canvas, data){
    // var canvas=document.getElementById("heatmap0");
    // console.log('enter heatmap')
    var heatmap = createWebGLHeatmap({canvas: canvas, intensityToAlpha:true});
    var conf=Template.popview.configure.heatmap;
    var col=conf.col, row=conf.row, width=conf.width, height=conf.height;

    for(var i=0;i<data.length;i++){
        var d=data[i];
        heatmap.addPoint(d.X*width/col, (row-d.Y-1)*height/row, 7, 0.7)
    }

    heatmap.update();
    heatmap.display();
}

var drawPop=function(g, data){

}

Template.popview.helpers({
})


Template.popview.events({
    'click #one_day_visitor': function(event) {
        checkMdsFilter(1)
    },
});

Meteor.startup(function() {
    Session.set('selectedTime', '2014-6-06 09:00:00')
})








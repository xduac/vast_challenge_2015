var Collection={};
Collection.mc1=new Meteor.Collection('mc1')
Collection.mds=new Meteor.Collection('mds')
Collection.timeline=new Meteor.Collection('timeline')
Collection.mdsFromConn=new Meteor.Collection('mdsFromConn')

var str2sec = function(str) {
    var inputFormat = 'YYYY-M-DD HH:mm:ss'
    var parse = moment(str, inputFormat);
    return parse.hour() * 3600 + parse.minute() * 60 + parse.second();
}
function secondsToTime(secs)
{
    var hour = Math.floor(secs / (60 * 60));
   
    var divisor_for_minutes = secs % (60 * 60);
    var minute = Math.floor(divisor_for_minutes / 60);
 
    var divisor_for_seconds = divisor_for_minutes % 60;
    var second = Math.ceil(divisor_for_seconds);
   
    if (hour < 10) hour = '0' + hour;
    if (minute < 10) minute = '0' + minute;
    if (second < 10) second = '0' + second;
    return '' + hour + ':' + minute + ':' + second;
}

Meteor.publish('singlePath', function(id){
	return Collection.mc1.find({id:id});
})

Meteor.publish('mds', function(day){
	return Collection.mds.find({day:day});
})


Meteor.publish('mdsFromConn', function(id, day){
	return Collection.mdsFromConn.find({id:id, day:day})
})

Meteor.publish('timePoint', function(time){
	var sec=str2sec(time);
	var day=time.split(' ')[0];
	var start=sec-15, end=sec+15;

	var istart=day+' '+secondsToTime(start);
	var iend=day+' '+secondsToTime(end);
	return Collection.mc1.find({
		'Timestamp':{$gt:istart, $lt:iend}
	});
})

Meteor.publish('timeline', function(day){
    var dateConst=['2014-6-06 00:00:00', '2014-6-07 00:00:00', '2014-6-08 00:00:00', '2014-6-09 00:00:00']
	return Collection.timeline.find({
		time:{$gt:dateConst[day], $lt:dateConst[day+1]}
	})
})


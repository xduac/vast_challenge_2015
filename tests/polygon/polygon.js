var circleNumber=5;
var circleNumberDep=new Tracker.Dependency;

Template.polygon.events({
	'change #edgeInput':function(events, template){
		circleNumber=document.querySelector('input[type=number]').value;
		circleNumberDep.changed();
	},
});

var getCircleNum=function(){
	circleNumberDep.depend();
	return circleNumber;
}

//r is the polygon radius
var getRfromN=function(n, r, padding){
	var R=(r+padding)/Math.sin(Math.PI/n);
	return R;
}

var getGradienXYfromAlpha=function(alpha){
	var x, y;
	var pi=Math.PI;
	var angle=alpha%(2*pi);

	if( angle>=pi/4 && angle<=3*pi/4 ){
		x=0.5-0.5/Math.tan(alpha);
		y=0;
	}else if(angle>=5*pi/4 && angle <=7*pi/4){
		x=0.5+0.5/Math.tan(alpha);
		y=1;
	}else if(angle>=3*pi/4 && angle <=5*pi/4){
		x=1;
		y=0.5+0.5*Math.tan(alpha);
	}else if(angle>=7*pi/4 || angle <=3*pi/4){
		x=0;
		y=0.5-0.5*Math.tan(alpha);
	}

	return {x:x, y:y};
}

var getLinksFromPoints=function(points){
    var r=Template.polygon.configure.circleRadius;//circle radius
    var PI=Math.PI;
    
	if(points.length==2){
		var alpha1=points[0].alpha;
		var alpha2=points[1].alpha;
		return [{
			source:{
				x:points[0].x ,
				y:points[0].y-r ,
			},
			target:{
				x:points[1].x,
				y:points[1].y-r,
			},
			linkId:Math.random(),
		}]
	}
	//prepare for verticles
	var verticle=points;
	var i;
	var startIdx;
	for(i=0;i<points.length;i++){
		if(points[i].alpha>Math.PI/2){
			startIdx=i;
			break;
		}
	}
	for(i=0;i<startIdx;i++){
		verticle.push(verticle.shift());
	}
	//start to create links
	var links=[];
	for(i=0;i<verticle.length-1;i++){
		var alpha1=verticle[i].alpha;
		var alpha2=verticle[i+1].alpha;
		var edge={
			source:{
				x:verticle[i].x+r*Math.cos(alpha1+PI/4) ,
				y:verticle[i].y+r*Math.sin(alpha1+PI/4) ,
			},
			target:{
				x:verticle[i+1].x+r*Math.cos(alpha2-PI/4) ,
				y:verticle[i+1].y+r*Math.sin(alpha2-PI/4) ,
			},
			linkId:Math.random(),
		}
		links.push(edge);
	}
	return links;
}

var drawPolyCircle=function(polygonPara, svg){

    var g; 
    //polygon radius 

    var r=Template.polygon.configure.circleRadius;//circle radius

    var centerX=polygonPara.centerX;
    var centerY=polygonPara.centerY;
    var n=polygonPara.n;
    var radius=polygonPara.radius;
    var flag=polygonPara.rotate;

	g = svg.append('g')
		.attr("class", "g")
		.attr('transform', "rotate(90, " + centerX + ", " + centerY + ")")
		.attr('transform', "translate(" + centerX + ", " + centerY + ")")

    var alpha, circleData, i, points, x, y, offset;

    x = radius;
    y = 0;
    offsetNum=0;
    circleData = (function() {
      var _results;
      _results = [];
      for (i = 1; 1 <= n ? i <= n : i >= n; 1 <= n ? i++ : i--) {
        alpha = i * 2 * Math.PI / n;
        var gradientXY=getGradienXYfromAlpha(alpha);
        x = radius * Math.cos(alpha);
        y = radius * Math.sin(alpha);
        _results.push({
          x: x,
          y: y,
          offset:(alpha%(2*Math.PI)/(2*Math.PI)),
          alpha:alpha,
          gx:gradientXY.x,
          gy:gradientXY.y,
        });
      }
      return _results;
    })();

    //define arrow
    // Per-type markers, as they don't inherit styles.
    var links=getLinksFromPoints(circleData);
    var	markerWidth = 8,
        markerHeight = 8,
        refX = 0 ,
		refY = 0,

    	drSub=r-r*(Math.sqrt(2)/2);

    g.append("svg:defs").selectAll("marker")
        .data(links)
        .enter().append("svg:marker")
    .attr("id", function(d){return 'arrow'+d.linkId;})
        .attr("viewBox", "0 -5 10 10")
        .attr("refX", refX+markerWidth)
        .attr("refY", refY)
        .attr("markerWidth", markerWidth)
        .attr("markerHeight", markerHeight)
        .attr("orient", "auto")
        .append("svg:path")
        .attr("d", "M0,-5L10,0L0,5");

    var path = g.append("svg:g").selectAll("path")
        .data(links)
        .enter().append("svg:path")
        .attr("class", function (d) {
	        return d.linkId;
	    })
        .attr("marker-end", function (d) {
	        return "url(#arrow" + d.linkId+ ")";
	    })
	    .attr("fill","none")
	    .attr("stroke","red")
	    .attr("stroke-width",1)
	    .attr("d", function (d) {
            var dx = d.target.x - d.source.x,
                dy = (d.target.y - d.source.y),
                dr = Math.sqrt(dx * dx + dy * dy);
            return "M" + d.source.x + "," + d.source.y + "A" + (dr - drSub) + "," + (dr - drSub) + " 0 0,1 " + d.target.x + "," + d.target.y;
        });

    points = g.selectAll('circle').data(circleData);
    
    points.enter().append('svg:circle').attr('r', r)
    .attr("class", "circle")
    .attr('cx', function(d) {
      return d.x;
    }).attr('cy', function(d) {
      return d.y;
    }).attr('alpha', function(d) {
      return d.alpha;
    }).attr('opacity', 1)
    .attr("fill", function(d){
	    var gradientId=Math.random();
		var gradient = g.append("svg:defs")
		    .append("svg:radialGradient")
		    .attr("class","gradient")
		    .attr("id", gradientId)
		    .attr("cx", "0.5")
		    .attr("cy", "0.5")
		    .attr("fx", d.gx)
		    .attr("fy", d.gy)
		    .attr("r", Math.sqrt(2)/2+0.0001)
		    .attr("spreadMethod", "pad");

			// Define the gradient colors
			gradient.append("svg:stop")
			    .attr("offset", (d.offset*100)+'%')
			    .attr("stop-color", "#a00000")
			    .attr("stop-opacity", 1);

			gradient.append("svg:stop")
			    .attr("offset", "100%")
			    .attr("stop-color", "#ffffff")
			    .attr("stop-opacity", 1);

		return "url(#"+gradientId+")";
	})

};


Template.polygon.rendered = function(){
	console.log("start to draw")

    var RADIUS=300;

	//Create SVG element
	var svg = d3.select("#polygonCircle")
				.attr("width", 3*RADIUS)
				.attr("height", 3*RADIUS);

	var testData=[ { name: 'node0', group: 2, timestamp: 1324 },
	  { name: 'node1', group: 1, timestamp: 1324 },
	  { name: 'node2', group: 3, timestamp: 1324 },
	  { name: 'node3', group: 1, timestamp: 1324 },
	  { name: 'node4', group: 0, timestamp: 1324 },
	  { name: 'node5', group: 3, timestamp: 1324 },
	  { name: 'node6', group: 2, timestamp: 1324 },
	  { name: 'node7', group: 0, timestamp: 1324 },
	  { name: 'node8', group: 1, timestamp: 1324 },
	  { name: 'node9', group: 2, timestamp: 1324 } ];

	var groups=d3.nest()
		.key(function(d){return d.group;})
		.entries(testData);

	console.log(groups)

	var middleLinePara={
		x1:0,
		x2:600,
		y1:300,
		y2:300,
	}

	for(var i=0;i<groups.length;i++){
		var group=groups[i];
		var groupName=group.key,
			groupSize=group.values.length;

		var padding=Template.polygon.configure.circlePadding;
	    var r=Template.polygon.configure.circleRadius;//circle radius

	    var radius=getRfromN(groupSize, r, padding);//polygon radius
	    var polygonPara={
	    	centerY:middleLinePara.y1-radius-r,
	    	centerX:(i+1)*150,
	    	radius:radius,
	    	n:groupSize,
	    	rotate:false,
	    }
		drawPolyCircle(polygonPara, svg);
	}

	svg.append("g").attr("transform","translate("+middleLinePara.x1+","+middleLinePara.y1+")")
	.append("line")
	.attr("x1",0)
	.attr("x2",600)
	.attr("y1",0)
	.attr("y2",0)
	.attr("stroke","black")
	.attr("stroke-width",3)



	// this.autorun(function(){

	    // var padding=Template.polygon.configure.circlePadding;
	    // var r=Template.polygon.configure.circleRadius;//circle radius
	    // var n=getCircleNum();//polygon edge number

	    // var radius=getRfromN(n, r, padding);//polygon radius
	    // var topleftX=20;//polygon top left x axis
	    // var topleftY=20;//polygon top left Y axis


	    // var centerX=topleftX+radius+r; //polygon center x axis
	    // var centerY=topleftY+radius+r; //polygon center y axis

	    // var polygonPara={
	    // 	centerX:centerX,
	    // 	centerY:centerY,
	    // 	radius:radius,
	    // 	n:n,
	    // };


		// d3.select("svg").selectAll(".g").remove();
		// return drawPolyCircle(polygonPara, svg);
	// });
};